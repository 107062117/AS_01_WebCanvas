var canvas, ctx;
var flag = false,
    prevX = 0 ,
    currX = 0,
    prevY = 0,
    currY = 0,
    dot_flag = false,
    hue = 0;
var x,
    y,
    s,
    f;
var hol;
var myfunc;
var cPushArray;
var cStep;
var imagearray;
var bgarray;
var iswriting;

function init() {
    canvas = document.getElementById('mycanvas');
    ctx = canvas.getContext("2d");
    text_init();
    cStep = -1;
    cPushArray = [];
    imagearray = [];
    cPush();
    x = document.getElementById('color').value;
    y = document.getElementById('bsize').value;
    f = document.getElementById('fsize').value;
    s = document.getElementById('ftype').options.value;
    hol = false;
    canvas.addEventListener("mousemove", function (e) {
        findxy('move', e)
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        findxy('down', e)
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        findxy('up', e)
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        findxy('out', e)
    }, false);
}

function func(val){
    switch(val){
        case "draw":
            myfunc="draw";
            canvas.style.cursor = "url('cursor1.png'), auto";
            buttonUsing('draw');
            break;
        case "erase":
            myfunc="erase";
            canvas.style.cursor = "url('cursor2.png'), auto";
            buttonUsing('erase');
            break;
        case "text":
            myfunc="text";
            canvas.style.cursor = "url('cursor3.png'), auto";
            buttonUsing('text');
            break;
        case "rect":
            myfunc="rect";
            canvas.style.cursor = "url('crosshair.png'), auto";
            buttonUsing('rectangle');
            break;
        case "circle":
            myfunc="circle";
            canvas.style.cursor = "url('circle-cursor.png'), auto";
            buttonUsing('circle');
            break;
        case "tria":
            myfunc="tria";
            canvas.style.cursor = "url('triangle-cursor.png'), auto";
            buttonUsing('triangle');
            break;
        case "line":
            myfunc="line";
            canvas.style.cursor = "url('crosshair.png'), auto";
            buttonUsing('line');
            break;
        case "marker":
            myfunc="marker";
            canvas.style.cursor = "url('marker-cursor.png'), auto";
            buttonUsing('marker');
            break;
        case "rainbow":
            myfunc="rainbow";
            canvas.style.cursor = "url('cursor1.png'), auto";
            buttonUsing('rainbow');
            break;
    }
    if (myfunc !== "marker")
        ctx.globalAlpha = 1;
}

function buttonUsing(id){
    var idlist = ['draw','marker','erase','text', 'rectangle', 'circle', 'triangle', 'line', 'rainbow'];
    for (var i = 0; i < idlist.length; i++){
        if (idlist[i] != id){
            document.getElementById(idlist[i]).style['border-color'] = "rgba(174, 248, 159, 1)";
            document.getElementById(idlist[i]).style.boxShadow = "none";
        }
        else{
            document.getElementById(id).style['border-color'] = "rgb(121, 245, 239)";
            document.getElementById(id).style.boxShadow = "0px 0px 25px rgb(0, 255, 170)"; 
        }
    }
}

function bgcolor(){
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 1;
    var color = document.getElementById('bgcolor').value;
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    cPush();
}

function hollow(){
    if (hol === true){
        hol = false;
        var h = document.getElementById('hollow');
        h.style['background-image'] = "url(fill.png)";
    }
    else{
        hol = true;
        var h = document.getElementById('hollow');
        h.style['background-image'] = "url(hollow.png)";
    }
}

function draw_cimage(){
    return new Promise(function(resolve, reject){
        var canvasPic = new Image();
        if (cStep >= 0) {
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function () {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0);
                resolve();
            }
        }
        else
            reject();
    });
}

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { 
        cPushArray.length = cStep; 
    }
    cPushArray.push(canvas.toDataURL());
    console.log("Push: frame"+cStep);
    var inform = {w:canvas.width, h:canvas.height};
    imagearray.push(inform);
}

function undo() {
    var canvasPic = new Image();
    if (cStep > 0) {
        cStep--;
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.width=imagearray[cStep].w;
            canvas.height=imagearray[cStep].h;
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
    else {
        alert('Nothing to undo!');
        canvasPic.onload = function () {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.width=imagearray[cStep].w;
            canvas.height=imagearray[cStep].h;
            ctx.drawImage(canvasPic.src, 0, 0);
        }
    }
}

function redo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.width = imagearray[cStep].w;
            canvas.height = imagearray[cStep].h;
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
    else{
        alert('Nothing to redo');
    }
    //console.log("redo: frame"+cStep);
}

function color() {
    x = document.getElementById('color').value;
}

function brushsize(){
    y = document.getElementById('bsize').value;
}
function fsize(){
    f = document.getElementById('fsize').value;
}

function ftype(){
    s = document.getElementById('ftype').value;
}

function draw() {
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    if (dot_flag) {
        ctx.beginPath();
        ctx.fillStyle = x;
        ctx.globalAlpha=1;
        ctx.fillRect(currX, currY+13, y, y);
        dot_flag = false;
    }
    if (flag){
        ctx.beginPath();
        ctx.moveTo(prevX, prevY+13);
        ctx.lineTo(currX, currY+13);
        ctx.globalAlpha=1;
        ctx.strokeStyle = x;
        ctx.lineWidth = y;
        ctx.stroke();
    }
}

function rainbow() {
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    if (flag){
        ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
        ctx.beginPath();
        ctx.moveTo(prevX, prevY+13);
        ctx.lineTo(currX, currY+13);
        ctx.lineWidth = y;
        ctx.globalAlpha = 1;
        ctx.stroke();
        hue++;
        if (hue >= 360) {
            hue = 0
        }
    }
}

function marker() {
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    if (flag){
        ctx.beginPath();
        ctx.moveTo(prevX, prevY+13);
        ctx.lineTo(currX, currY+13);
        ctx.strokeStyle = x;
        ctx.globalAlpha = 0.2;
        ctx.lineWidth = y;
        ctx.stroke();
    }
}

function erase(){
    ctx.globalCompositeOperation = 'destination-out';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.moveTo(prevX, prevY+13);
    ctx.lineTo(currX, currY+13);
    ctx.globalAlpha=1;
    ctx.strokeStyle = x;
    ctx.lineWidth = y;
    ctx.stroke();
}

function circle(){
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.moveTo(prevX, prevY + (currY - prevY) / 2);
    ctx.bezierCurveTo(prevX, prevY, currX,prevY, currX, prevY + (currY - prevY) / 2);
    ctx.bezierCurveTo(currX, currY, prevX, currY, prevX, prevY + (currY - prevY) / 2);
    ctx.strokeStyle = x;
    ctx.fillStyle = x;
    ctx.lineWidth = y;
    ctx.globalAlpha=1;
    if (!hol) ctx.fill();
    else ctx.stroke();
}

function rect() {
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.rect(prevX, prevY, currX - prevX, currY-prevY);
    ctx.strokeStyle = x;
    ctx.fillStyle = x;
    ctx.lineWidth = y;
    ctx.globalAlpha=1;
    if (!hol) ctx.fill();
    else ctx.stroke();
}

function line(){
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = x;
    ctx.lineWidth = y;
    ctx.globalAlpha=1;
    ctx.stroke();
}

function text_init(){
    ctx.globalCompositeOperation = 'source-over';
    var input = document.getElementById("text_place");
    input.style.visibility="hidden";
    input.style.width="80px";
    input.style.height="20px";
    input.style.border="1px solid rgb(161, 151, 252)";
    input.style['z-index'] = 4;
    input.style.position="absolute";
    input.style.top= "0px";
    input.style.left="0px";
    iswriting = false;
}

function text(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 1;
    var input = document.getElementById("text_place");
    input.value="";
    input.style.visibility="visible";
    input.style.position="absolute";
    input.style.top = e.pageY+"px";
    input.style.left = e.pageX+"px";
    iswriting = true;
}

function key(event){
    if (event.keyCode === 13){
        if (iswriting){
            var input = document.getElementById("text_place");
            ctx.font = f + 'pt '+ s;
            ctx.fillstyle = x;
            ctx.fillText(input.value, currX, currY+13);
            input.style.visibility="hidden";
            iswriting = false;
            cPush();
        }
    }
}

function text_fill(tx, ty){
    if (iswriting){
        var input = document.getElementById("text_place");
        ctx.font = f + 'pt '+ s ;
        ctx.fillstyle = x;
        ctx.fillText(input.value, tx, ty);
        input.style.visibility="hidden";
        iswriting = false;
    }
}

function tria(){
    ctx.globalCompositeOperation = 'source-over';
    ctx.lineJoin ='round';
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(prevX-Math.abs((currX-prevX)/Math.sqrt(3)), currY);
    ctx.lineTo(prevX+Math.abs((currX-prevX)/Math.sqrt(3)), currY);
    ctx.closePath();
    ctx.strokeStyle = x;
    ctx.fillStyle = x;
    ctx.lineWidth = y;
    ctx.globalAlpha=1;
    if (!hol) ctx.fill();
    else ctx.stroke();
}

function reset(){
    if (confirm("Sure to clean?")){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvas.width=600;
        canvas.height=460;
        cPush();
    }
}

function download() {
    var d = document.createElement('a');
    var img = canvas.toDataURL();
    d.href = img;
    d.download = "new_canvas.png";
    d.click();
}

function img(){
    ctx.globalCompositeOperation = 'source-over';
    document.getElementById('img').onchange = function(e) {
        var img = new Image();
        img.onload = function(){
            canvas.width = this.width;
            canvas.height = this.height;
            ctx.drawImage(this, 0, 0, this.width, this.height);
            cPush();
        };
        img.src = URL.createObjectURL(this.files[0]);
        this.value="";
    };
}

async function Dcircle(){
    await draw_cimage();
    circle();
}
async function Drect(){
    await draw_cimage();
    rect();
}
async function Dtria(){
    await draw_cimage();
    tria();
}
async function Dline(){
    await draw_cimage();
    line();
}

function findxy(res, e) {
    var leftpart=document.getElementById('leftpart');
    if (res == 'down') {
        prevX = currX;
        prevY = currY;
        currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
        currY = e.pageY - canvas.offsetTop;
        flag = true;
        dot_flag = true;
        if (myfunc==="draw") draw();
        else if(myfunc === "rainbow") {
            hue = 0;
            ctx.strokeStyle = '#BADA55';
        }
        else if (myfunc==="erase") erase();
        else if (myfunc === 'text'){
            if(!iswriting || document.getElementById("text_place").value==="")
                text(e);
            else text_fill(prevX, prevY);
        }
    }
    if (res == 'up'){
        cPush();
        flag = false;
    } 
    if (res == 'out') {
        flag = false;
    }
    if (res == 'move') {
        if (myfunc==="draw"){
            prevX = currX;
            prevY = currY;
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) draw();
        }
        else if (myfunc==="marker"){
            prevX = currX;
            prevY = currY;
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) marker();
        }
        else if (myfunc==="erase"){
            prevX = currX;
            prevY = currY;
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) erase();
        }
        else if (myfunc==="line"){
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) {Dline();}
        }
        else if (myfunc==="rect"){
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) {Drect();}
        }
        else if (myfunc==="circle"){
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) {Dcircle();}
        }
        else if (myfunc==="tria"){
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag) {Dtria();}
        }
        else if(myfunc==="rainbow"){
            prevX = currX;
            prevY = currY;
            currX = e.pageX - leftpart.offsetLeft - canvas.offsetLeft;
            currY = e.pageY - canvas.offsetTop;
            if (flag)rainbow();
        }
    }
}

