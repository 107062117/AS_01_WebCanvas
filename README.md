# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

---
#### Tool-Bar
- Font-type
`選擇字形`
- Font-size
`選擇字型大小`
- Color
`選擇顏色`
- Brush-size
`選擇筆刷大小`
---
#### Tool-Box

![](https://i.imgur.com/4BAOUxr.png =30x30)`筆刷，可在繪圖區隨意作圖。`

![](https://i.imgur.com/O7R2NzS.png =30x30) `螢光筆，可在繪圖區任意作圖。`

![](https://i.imgur.com/3BQKdEJ.png =30x30) `橡皮擦，可清除經過區域的圖案`

![](https://i.imgur.com/Tv8w6qI.png =30x30) `彩色畫筆，畫圖時依彩虹顏色變化畫筆顏色`


![](https://i.imgur.com/umNvFJz.png =30x30) `畫直線`

![](https://i.imgur.com/mDaLHhW.png =30x30) `畫矩形`

![](https://i.imgur.com/dJaYqbb.png =30x30) `畫圓形`

![](https://i.imgur.com/lGhKiZe.png =30x30) `畫三角形`

![](https://i.imgur.com/CwKP4PN.png =30x30) / ![](https://i.imgur.com/t4NIEsF.png =30x30)`點擊可切換矩形、圓形、三角形為中空或填滿`

![](https://i.imgur.com/Q5pbyjx.png =30x30) `點擊繪圖區任意位置可在該位置輸入文字`

![](https://i.imgur.com/Aikyimb.png =30x30)`背景填滿`

![](https://i.imgur.com/sR4vOz5.png =30x30) `上傳圖片至繪圖區`

![](https://i.imgur.com/m5LaRu7.png =30x30) `回上一步`

![](https://i.imgur.com/8AUFosw.png =30x30) `回下一步`

![](https://i.imgur.com/VxThKGf.png =30x30) `清空繪圖區`


![](https://i.imgur.com/oBqq1CM.png =30x30) `下載目前繪圖`

---
### Function description

![](https://i.imgur.com/Aikyimb.png =30x30)`背景填滿`

![](https://i.imgur.com/CwKP4PN.png =30x30) / ![](https://i.imgur.com/t4NIEsF.png =30x30)`點擊可切換矩形、圓形、三角形為中空或填滿`

![](https://i.imgur.com/O7R2NzS.png =30x30) `螢光筆，可在繪圖區任意作圖。`

![](https://i.imgur.com/umNvFJz.png =30x30) `畫直線`

![](https://i.imgur.com/Tv8w6qI.png =30x30) `彩色畫筆，畫圖時依彩虹顏色變化畫筆顏色`

---
### Gitlab page link

    https://107062117.gitlab.io/AS_01_WebCanvas


### Others (Optional)

`Have fun with it!`

<style>
table th{
    width: 100%;
}
</style>